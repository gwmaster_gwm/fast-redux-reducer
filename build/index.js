"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkBindAction = checkBindAction;
exports.mapActionsToProps = mapActionsToProps;
exports.mapStoreToProps = mapStoreToProps;
exports.autoReducer = autoReducer;
exports.globalReducer = void 0;

var _redux = require("redux");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var globalReducer = function globalReducer(actionName) {
  var defaultPayload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var action = arguments.length > 1 ? arguments[1] : undefined;

    if (action.type == actionName) {
      if (action.payload === undefined) {
        console.error(actionName + ' payload is undefinelsd ', action);
        return null;
      }

      return action.payload;
    }

    if (state == null) {
      return defaultPayload;
    }

    return state;
  };
};

exports.globalReducer = globalReducer;

function mapActionsToProps() {
  for (var _len = arguments.length, actionsList = new Array(_len), _key = 0; _key < _len; _key++) {
    actionsList[_key] = arguments[_key];
  }

  return function (dispatch) {
    return (0, _redux.bindActionCreators)(_objectSpread({}, checkBindAction.call.apply(checkBindAction, [checkBindAction].concat(actionsList))), dispatch);
  };
}

function mapStoreToProps() {
  for (var _len2 = arguments.length, reducerList = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    reducerList[_key2] = arguments[_key2];
  }

  var reducersListStr = reducerList.join(',');
  return new Function("return function f( {".concat(reducersListStr, "}){ return {").concat(reducersListStr, "}  };"))();
}

function checkBindAction() {
  var bindActions = {};
  var error = false;

  for (var _len3 = arguments.length, actionsList = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    actionsList[_key3] = arguments[_key3];
  }

  for (var actions in actionsList) {
    for (var fn in actionsList[actions]) {
      if (bindActions[fn] != undefined) {
        alert("Develop bug same action name ".concat(fn));
        console.error("Same action name ".concat(fn));
      }

      bindActions[fn] = actionsList[actions][fn];
    }
  }

  return bindActions;
}

function autoReducer(reducersList) {
  var reducers = {};

  for (var red in reducersList) {
    for (var varName in reducersList[red]) {
      if (reducers[varName] != undefined) {
        console.log(varName);
        alert('store have var this name ' + varName);
      }

      reducers[varName] = reducersList[red][varName];

      if (Array.isArray(reducers[varName])) {
        // use default reducer
        var arr = reducers[varName];
        reducers[varName] = globalReducer(arr[0], arr[1]);
      }
    }
  }

  return reducers;
}
//# sourceMappingURL=index.js.map