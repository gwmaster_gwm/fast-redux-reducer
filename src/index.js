import { bindActionCreators } from 'redux'

const globalReducer = (actionName, defaultPayload = null) => (state = null, action) => {
  if (action.type == actionName) {
    if (action.payload === undefined) {
      console.error(actionName + ' payload is undefinelsd ', action)
      return null

    }
    return action.payload
  }
  if (state == null) {
    return defaultPayload
  }
  return state
}

function mapActionsToProps (...actionsList) {
  return function (dispatch) {
    return bindActionCreators({ ...checkBindAction.call(checkBindAction, ...actionsList) }, dispatch)
  }
}

function mapStoreToProps (...reducerList) {
  let reducersListStr = reducerList.join(',')
  return new Function(`return function f( {${reducersListStr}}){ return {${reducersListStr}}  };`)()
}

function checkBindAction (...actionsList) {
  let bindActions = {}
  let error = false
  for (let actions in actionsList) {
    for (let fn in actionsList[actions]) {
      if (bindActions[fn] != undefined) {
        alert(`Develop bug same action name ${fn}`)
        console.error(`Same action name ${fn}`)
      }
      bindActions[fn] = actionsList[actions][fn]
    }
  }
  return bindActions
}

function autoReducer (reducersList) {
  let reducers = {}
  for (let red in reducersList) {
    for (let varName in reducersList[red]) {
      if (reducers[varName] != undefined) {
        console.log(varName)
        alert('store have var this name ' + varName)
      }
      reducers[varName] = reducersList[red][varName]
      if (Array.isArray(reducers[varName])) {
        // use default reducer
        let arr = reducers[varName]
        reducers[varName] = globalReducer(arr[0], arr[1])
      }
    }
  }
  return reducers
}

export{
  globalReducer,
  checkBindAction,
  mapActionsToProps,
  mapStoreToProps,
  autoReducer
}